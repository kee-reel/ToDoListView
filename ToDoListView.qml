import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "popups"

Item {
    id: root
    property var windowSize: width > height ? height : width
    property var parentData: taskModel.getVariantItem(taskModel.parentId)

    signal onIndexSelected(int index)

    function transitToIndex(index) {
        onIndexSelected(index)
    }

    function openItemEdit(itemId, modelData) {
        editForm.openEdit(itemId, modelData)
    }

    function formatText(count, modelData) {
        var data = count === 12 ? modelData + 1 : modelData
        return data.toString().length < 2 ? "0" + data : data
    }

    function close() {
        if (addForm.visible)
            addForm.close()
        else if (editForm.visible)
            editForm.close()
        else if (taskModel.parentId === -1)
            uiElement.closeSelf()
        else
            transitToIndex(-1)
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            close()
            event.accepted = true
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#3b4252"
    }

    Item {
        id: header
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 10 * ratio
        width: parent.width
        height: 70 * ratio
        Rectangle {
            visible: taskModel.parentId !== -1
            height: parent.height
            width: parent.width
            color: "#81a1c1"
            Text {
                id: name
                text: taskModel.parentId
                      !== -1 ? parentData["IUserTaskDataExtention/1.0"]["name"] : ""
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                font.pixelSize: 30 * ratio
            }
        }
        Button {
            id: exitButton
            width: parent.height
            height: width
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 10 * ratio
            icon.source: "qrc:/Icons/back.png"
            icon.color: "#eceff4"
            icon.width: width / 2
            icon.height: icon.width
            onClicked: {
                close()
            }
            background: Rectangle {
                color: exitButton.pressed ? "#33000000" : "transparent"
                radius: 90
            }
        }
    }

    ScrollView {
        id: scrollView
        clip: true
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        contentWidth: width
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOff

        ListView {
            id: listView
            anchors.fill: parent
            model: taskModel
            boundsBehavior: Flickable.StopAtBounds
            headerPositioning: ListView.OverlayHeader
            spacing: 4 * ratio

            delegate: ListButton {
                width: parent.width - spacing * 2
                anchors.horizontalCenter: parent.horizontalCenter
            }

            //				section.property: "isDone"
            //				section.criteria: ViewSection.FirstCharacter
            //				section.delegate: ToolBar {
            //					id: background
            //					width: listView.width
            //					Label {
            //						id: label
            //						text: section ? "Completed" : "Active"
            //						anchors.fill: parent
            //						horizontalAlignment: Qt.AlignLeft
            //						verticalAlignment: Qt.AlignVCenter
            //						leftPadding: 20
            //						font.pointSize: 13
            //						font.bold: true
            //					}
            //				}
            remove: Transition {
                NumberAnimation {
                    property: "opacity"
                    to: 0
                    easing.type: Easing.InCubic
                }
            }

            addDisplaced: Transition {
                NumberAnimation {
                    property: "y"
                    easing.type: Easing.InOutQuad
                }
            }

            displaced: Transition {
                NumberAnimation {
                    property: "y"
                    easing.type: Easing.InOutQuad
                }
            }

            footer: Rectangle {
                color: "transparent"
                width: parent.width
                height: scrollView.height / 2
            }

            OptionsList {
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.margins: width / 5
                width: 70 * ratio
                height: width
                id: addButtons
                optionName: ""
                icon.source: "qrc:/Icons/plus.png"
                icon.color: "#eceff4"
                icon.width: width / 2
                icon.height: icon.width

                toLeft: true
                optionsWidth: width * 2
                optionsModel: ListModel {
                    ListElement {
                        name: "Task"
                    }
                    ListElement {
                        name: "Project"
                    }
                }

                Component.onCompleted: {
                    optionPicked.connect(onOptionPicked)
                }
                function onOptionPicked(itemData) {
                    addForm.openAdd(itemData.name !== "Task")
                }
            }
        }
    }

    Rectangle {
        visible: false
        anchors.fill: parent
        color: "#66000000"

        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            onClicked: {
                addForm.close()
                visible = false
            }
        }

        AddForm {
            id: addForm
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            Component.onCompleted: {
                onOpenedChanged.connect(openedChanged)
            }
            function openedChanged(isOpened) {
                parent.visible = isOpened
            }
        }
    }

    EditForm {
        id: editForm
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    //    Rectangle {
    //		visible: false
    //        anchors.fill: parent
    //        color: "#66000000"
    //        DatePicker {
    //            id: datePicker
    //            anchors.fill: parent
    //            anchors.margins: 60
    //            property var callback: null
    //            visible: false
    //            Component.onCompleted: {
    //				onOpen.connect(onOpenSignal)
    //                onClose.connect(onCloseSignal)
    //            }

    //            function onOpenSignal() {
    //                parent.visible = true
    //            }

    //            function onCloseSignal(isOK) {
    //                if (isOK && callback !== null) {
    //                    callback()
    //                    callback = null
    //                }
    //                parent.visible = false
    //            }
    //        }
    //    }
    Rectangle {
        visible: false
        anchors.top: header.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        color: "#66000000"
        TimePicker {
            id: timePicker
            anchors.fill: parent
            anchors.margins: 60
            property var callback: null
            visible: false
            Component.onCompleted: {
                onOpen.connect(onOpenSignal)
                onClose.connect(onCloseSignal)
            }

            function onOpenSignal() {
                parent.visible = true
            }

            function onCloseSignal(isOK) {
                if (isOK && callback !== null) {
                    callback()
                    callback = null
                }
                parent.visible = false
            }
        }
    }
}
