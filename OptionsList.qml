import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Button {
    property string optionName: ""
    property var optionsModel: null
    property var optionsWidth: width
    property bool toLeft: false

    signal optionPicked(var itemData)

    text: optionName
    font.pixelSize: 30 * ratio
    background: Rectangle {
        color: parent.pressed ? "#88c0d0" : "#81a1c1"
        radius: height / 2
    }
    onClicked: optionsList.visible = !optionsList.visible

    ListView {
        id: optionsList
        anchors.left: toLeft ? null : parent.left
        anchors.right: !toLeft ? null : parent.right
        anchors.bottom: parent.top
        visible: false
        interactive: false
        width: optionsWidth
        height: children[0].height
        model: optionsModel
        delegate: optionButton
    }

    Component {
        id: optionButton
        Button {
            width: parent.width
            text: name
            font.pixelSize: 30 * ratio
            onClicked: {
                optionsList.visible = false
                optionPicked(model)
            }
            background: Rectangle {
				color: parent.pressed ? "#88c0d0" : "#eceff4"
			}
        }
    }
}
