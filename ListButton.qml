import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

SwipeDelegate {
    id: delegate
    height: windowSize / 7
//    DropShadow {
//		z: -1
//        anchors.fill: itemBackground
//        horizontalOffset: 1
//        verticalOffset: 1
//        radius: 8.0
//        samples: 17
//        color: "#80000000"
//        source: itemBackground
//    }

    background: Rectangle {
		id: itemBackground
        color: "#4c566a"
        radius: height / 20
    }
    
    onClicked: {
        openItemEdit(model.id, model)
    }
    contentItem: RowLayout {
        Button {
            implicitHeight: delegate.height * 0.66
            implicitWidth: implicitHeight
            background: Rectangle {
                color: "transparent"
            }
            icon.source: model.isProject ? "qrc:/Icons/list.png" : "qrc:/Icons/radio_btn.png"
            icon.color: "#81a1c1"
            icon.height: height
            icon.width: height

            Button {
                visible: !model.isProject
                id: buttonCheckMark
                background: Rectangle {
                    color: "transparent"
                }
                anchors.fill: parent
                icon.source: "qrc:/Icons/ic_done_black_24dp.png"
                icon.color: "#eceff4"
                icon.height: height
                icon.width: height
                scale: model.isDone ? 1 : 0
            }

            Button {
                visible: !model.isProject
                checkable: true
                checked: model.isDone
                anchors.fill: parent
                background: Rectangle {
                    color: "transparent"
                }
                onClicked: {
                    model.isDone = checked
                    completeAnimation.from = checked ? 0 : 1
                    completeAnimation.to = checked ? 1 : 0
                    completeAnimation.start()
                }
                PropertyAnimation {
                    id: completeAnimation
                    target: buttonCheckMark
                    property: "scale"
                    duration: 100
                    easing.type: Easing.InSine
                }
            }
        }
        Text {
            text: model.name
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 20 * ratio
            color: "#eceff4"
        }
        Button {
            visible: model.isProject
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            implicitHeight: parent.height
            implicitWidth: implicitHeight
            icon.source: "qrc:/Icons/forward.png"
            icon.color: "#eceff4"
            icon.width: implicitHeight / 2
            icon.height: implicitHeight / 2
            background: Rectangle {
                color: parent.pressed ? "#88c0d0" : "#81a1c1"
                radius: 90
            }
            onClicked: {
                if (model.isProject) {
                    transitToIndex(index)
                }
            }
        }
    }

    swipe.right: Rectangle {
        width: parent.width
        height: parent.height
        clip: true
        color: "#bf616a"

        Button {
            height: parent.height
            width: height
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "qrc:/Icons/ic_delete_black_24dp.png"
            icon.color: "white"
            icon.height: height
            icon.width: icon.height
            opacity: -delegate.swipe.position
            background: Rectangle {
                color: "transparent"
            }
        }

        SwipeDelegate.onClicked: {
            stopOperation(swipe.rightItem)
        }
        SwipeDelegate.onPressedChanged: {
            stopOperation(swipe.rightItem)
        }

        Timer {
            id: undoTimer
            interval: 2000
            onTriggered: taskModel.removeItem(model.id)
        }

        Rectangle {
            id: deleteProgress
            height: parent.height
            width: 0
            color: "red"
            opacity: 0.5
        }

        PropertyAnimation {
            id: animateColor
            target: deleteProgress
            properties: "width"
            to: parent.width
            duration: undoTimer.interval
            easing: Easing.Linear
        }

        function startOperation() {
            animateColor.start()
            undoTimer.start()
        }

        function stopOperation() {
            delegate.swipe.close()
            undoTimer.stop()
            animateColor.stop()
            deleteProgress.width = 0
        }
    }

    swipe.left: Rectangle {
        width: parent.width
        height: parent.height
        clip: true
        color: "#a3be8c"

        Button {
            height: parent.height
            width: height
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            icon.source: "qrc:/Icons/today.png"
            icon.color: "white"
            icon.height: height
            icon.width: icon.height
            opacity: delegate.swipe.position
            background: Rectangle {
                color: "transparent"
            }
        }

        SwipeDelegate.onClicked: {
            stopOperation(swipe.leftItem)
        }
        SwipeDelegate.onPressedChanged: {
            stopOperation(swipe.leftItem)
        }
    }

    swipe.transition: Transition {
        SmoothedAnimation {
            velocity: 5
            easing.type: Easing.InCirc
        }
    }

    swipe.onOpened: {
        swipe.rightItem.startOperation()
    }
}
