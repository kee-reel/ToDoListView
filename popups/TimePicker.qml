// ekke (Ekkehard Gentz) @ekkescorner
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
//import Qt.labs.calendar 1.0
import QtQuick.Controls.Material 2.12

Rectangle {
    id: timePicker
    property bool isLandscape: false
    property alias titleText: headerPane.titleText
    Material.elevation: 6
    visible: false

    color: Material.background

    property bool isOK: false

    signal onOpen
    signal onClose(bool isOk)

    function open() {
        visible = true
        isOK = false
        onOpen()
    }

    function close() {
        visible = false
        onClose(isOK)
    }

    // c1: circle 1 (outside) 1-12
    // c2: circle 2 (inside) 13-00
    // d: work day (outside) 7-18
    // n: night (inside) 19-6
    // m: minutes
    // q: only quarters allowed for minutes, disable the other ones
    // data model used to display labels on picker circles
    property var timePickerModel: [{
            "c1": "12",
            "c2": "00",
            "d": "12",
            "n": "00",
            "m": "00",
            "q": true
        }, {
            "c1": "1",
            "c2": "13",
            "d": "13",
            "n": "1",
            "m": "05",
            "q": false
        }, {
            "c1": "2",
            "c2": "14",
            "d": "14",
            "n": "2",
            "m": "10",
            "q": false
        }, {
            "c1": "3",
            "c2": "15",
            "d": "15",
            "n": "3",
            "m": "15",
            "q": true
        }, {
            "c1": "4",
            "c2": "16",
            "d": "16",
            "n": "4",
            "m": "20",
            "q": false
        }, {
            "c1": "5",
            "c2": "17",
            "d": "17",
            "n": "5",
            "m": "25",
            "q": false
        }, {
            "c1": "6",
            "c2": "18",
            "d": "18",
            "n": "6",
            "m": "30",
            "q": true
        }, {
            "c1": "7",
            "c2": "19",
            "d": "7",
            "n": "19",
            "m": "35",
            "q": false
        }, {
            "c1": "8",
            "c2": "20",
            "d": "8",
            "n": "20",
            "m": "40",
            "q": false
        }, {
            "c1": "9",
            "c2": "21",
            "d": "9",
            "n": "21",
            "m": "45",
            "q": true
        }, {
            "c1": "10",
            "c2": "22",
            "d": "10",
            "n": "22",
            "m": "50",
            "q": false
        }, {
            "c1": "11",
            "c2": "23",
            "d": "11",
            "n": "23",
            "m": "55",
            "q": false
        }]
    // this model used to display selected time
    // so you can add per ex. AM, PM or so
    property var timePickerDisplayModel: [{
            "c1": "12",
            "c2": "00",
            "d": "12",
            "n": "00",
            "m": "00",
            "q": true
        }, {
            "c1": "01",
            "c2": "13",
            "d": "13",
            "n": "01",
            "m": "05",
            "q": false
        }, {
            "c1": "02",
            "c2": "14",
            "d": "14",
            "n": "02",
            "m": "10",
            "q": false
        }, {
            "c1": "03",
            "c2": "15",
            "d": "15",
            "n": "03",
            "m": "15",
            "q": true
        }, {
            "c1": "04",
            "c2": "16",
            "d": "16",
            "n": "04",
            "m": "20",
            "q": false
        }, {
            "c1": "05",
            "c2": "17",
            "d": "17",
            "n": "05",
            "m": "25",
            "q": false
        }, {
            "c1": "06",
            "c2": "18",
            "d": "18",
            "n": "06",
            "m": "30",
            "q": true
        }, {
            "c1": "07",
            "c2": "19",
            "d": "07",
            "n": "19",
            "m": "35",
            "q": false
        }, {
            "c1": "08",
            "c2": "20",
            "d": "08",
            "n": "20",
            "m": "40",
            "q": false
        }, {
            "c1": "09",
            "c2": "21",
            "d": "09",
            "n": "21",
            "m": "45",
            "q": true
        }, {
            "c1": "10",
            "c2": "22",
            "d": "10",
            "n": "22",
            "m": "50",
            "q": false
        }, {
            "c1": "11",
            "c2": "23",
            "d": "11",
            "n": "23",
            "m": "55",
            "q": false
        }]

    // set these properties before start
    property int outerButtonIndex: 0
    property int innerButtonIndex: -1
    property bool pickMinutes: false
    property bool useWorkTimes: false
    property bool onlyQuartersAllowed: false
    property bool autoSwapToMinutes: false

    property string hrsDisplay: "12"
	property int hrsArrowAngle: 360 * (hrsDisplay % 12 / 12)
    property string minutesDisplay: "00"
	property int minutesArrowAngle: 360 * (minutesDisplay % 60 / 60)

    // opening TimePicker with a given HH:MM value
    // ATTENTION TimePicker is rounding DOWN to next lower 05 / 15 Minutes
    // if you want to round UP do it before calling this function
    function setDisplay(hhmm, q, w) {
        onlyQuartersAllowed = q
        useWorkTimes = w
        var s = hhmm.split(":")
        if (s.length == 2) {
            var hours = s[0]
            var minutes = s[1]
            if (onlyQuartersAllowed) {
                minutes = minutes - minutes % 15
            } else {
                minutes = minutes - minutes % 5
            }
            showMinutes(minutes)
            showHour(hours)
            checkDisplay()
        }
    }

    function showHour(hour) {
        for (var i = 0; i < timePickerDisplayModel.length; i++) {
            var h = timePickerDisplayModel[i]
            if (useWorkTimes) {
                if (h.d == hour) {
                    pickMinutes = false
                    innerButtonIndex = -1
                    outerButtonIndex = i
                    updateDisplayHour()
                    return
                }
                if (h.n == hour) {
                    pickMinutes = false
                    outerButtonIndex = -1
                    innerButtonIndex = i
                    updateDisplayHour()
                    return
                }
            } else {
                if (h.c1 == hour) {
                    pickMinutes = false
                    innerButtonIndex = -1
                    outerButtonIndex = i
                    updateDisplayHour()
                    return
                }
                if (h.c2 == hour) {
                    pickMinutes = false
                    outerButtonIndex = -1
                    innerButtonIndex = i
                    updateDisplayHour()
                    return
                }
            }
        }
        // not found
        console.log("Minutes not found: " + hour)
        pickMinutes = false
        innerButtonIndex = -1
        outerButtonIndex = 0
        updateDisplayHour()
    }
    function updateDisplayHour() {
        if (innerButtonIndex >= 0) {
            if (useWorkTimes) {
                hrsDisplay = timePickerDisplayModel[innerButtonIndex].n
            } else {
                hrsDisplay = timePickerDisplayModel[innerButtonIndex].c2
            }
            return
        }
        if (timePicker.useWorkTimes) {
            hrsDisplay = timePickerDisplayModel[outerButtonIndex].d
        } else {
            hrsDisplay = timePickerDisplayModel[outerButtonIndex].c1
        }
    }

    function showMinutes(minutes) {
        for (var i = 0; i < timePickerDisplayModel.length; i++) {
            var m = timePickerDisplayModel[i]
            if (m.m == minutes) {
                innerButtonIndex = -1
                outerButtonIndex = i
                pickMinutes = true
                updateDisplayMinutes()
                return
            }
        }
        // not found
        console.log("Minutes not found: " + minutes)
        innerButtonIndex = -1
        outerButtonIndex = 0
        pickMinutes = true
        updateDisplayMinutes()
        return
    } // showMinutes
    function updateDisplayMinutes() {
        minutesDisplay = timePickerDisplayModel[outerButtonIndex].m
    }

    function checkDisplay() {
        if (pickMinutes) {
            hrsButton.checked = false
            minutesButton.checked = true
        } else {
            minutesButton.checked = false
            hrsButton.checked = true
        }
    }

    function onMinutesButtonClicked() {
        hrsButton.checked = false
        minutesButton.checked = true
        timePicker.pickMinutes = true
        timePicker.showMinutes(timePicker.minutesDisplay)
    }
    function onHoursButtonClicked() {
        minutesButton.checked = false
        hrsButton.checked = true
        timePicker.pickMinutes = false
        timePicker.showHour(timePicker.hrsDisplay)
    }

    ColumnLayout {
        anchors.fill: parent
        Pane {
            id: headerPane
            property string titleText: qsTr("Time (HH:MM)")
            padding: 0
			Layout.alignment: Qt.AlignHCenter
            background: Rectangle {
                color: Material.background
            }

            GridLayout {
                id: headerGrid
                Layout.fillWidth: true
                property int myPointSize: isLandscape ? 48 : 64
                anchors.centerIn: parent
                rows: isLandscape ? 4 : 2
                columns: isLandscape ? 1 : 3
                rowSpacing: 0

                Label {
                    id: titleLabel
                    Layout.fillWidth: true
                    Layout.maximumWidth: isLandscape ? parent.width * 0.9 : parent.width
                    Layout.columnSpan: isLandscape ? 1 : 3
                    text: headerPane.titleText
                    color: Material.foreground
                    Layout.alignment: Text.AlignHCenter
                    font.pointSize: 32
                    fontSizeMode: Text.Fit
                }

                Button {
                    id: hrsButton
                    focusPolicy: Qt.NoFocus
                    Layout.alignment: isLandscape ? Text.AlignHCenter : Text.AlignRight
                    checked: true
                    checkable: true
                    contentItem: Label {
                        text: timePicker.hrsDisplay
                        font.pointSize: headerGrid.myPointSize
                        fontSizeMode: Text.Fit
                        opacity: hrsButton.checked ? 1.0 : 0.6
                        color: Material.foreground
                        elide: Text.ElideRight
                    }
                    background: Rectangle {
                        color: "transparent"
                    }
                    onClicked: {
                        onHoursButtonClicked()
                    }
                } // hrsButton

                Label {
                    text: ":"
                    Layout.alignment: Text.AlignHCenter
                    font.pointSize: headerGrid.myPointSize
                    fontSizeMode: Text.Fit
                    opacity: 0.6
                    color: Material.foreground
                }

                Button {
                    id: minutesButton
                    focusPolicy: Qt.NoFocus
                    Layout.alignment: isLandscape ? Text.AlignHCenter : Text.AlignLeft
                    checked: false
                    checkable: true
                    contentItem: Label {
                        text: timePicker.minutesDisplay
                        font.pointSize: headerGrid.myPointSize
                        fontSizeMode: Text.Fit
                        opacity: minutesButton.checked ? 1.0 : 0.6
                        color: Material.foreground
                        elide: Text.ElideRight
                    }
                    background: Rectangle {
                        color: "transparent"
                    }
                    onClicked: {
                        onMinutesButtonClicked()
                    }
                } // hrsButton
            } // header grid
        } // headerPane

        Pane {
            id: timeButtonsPane
            implicitWidth: parent.width
            implicitHeight: implicitWidth
            padding: 0

            background: Rectangle {
                color: Material.background
            }

            Rectangle {
                anchors.fill: parent
                color: Material.color(Material.Grey, Material.Shade50)
                radius: width / 2
            }

            Button {
                text: qsTr("Now")
                anchors.right: parent.left
                anchors.top: parent.top
                onClicked: {
                    timePicker.setDisplay(Qt.formatTime(new Date(), "HH:mm"),
                                          timePicker.onlyQuartersAllowed,
                                          timePicker.useWorkTimes)
                }
            }

            ButtonGroup {
                id: buttonGroup
            }

            Pane {
                id: innerButtonsPane
                padding: 0
                visible: !timePicker.pickMinutes
                anchors.fill: parent
                background: Rectangle {
                    color: "transparent"
                }

                Repeater {
                    id: innerRepeater
                    model: timePicker.timePickerModel
                    delegate: Button {
                        id: innerButton
                        focusPolicy: Qt.NoFocus
                        text: timePicker.useWorkTimes ? modelData.n : modelData.c2
                        font.bold: checked
                        anchors.centerIn: parent
                        width: 100
                        height: width
                        checked: index == timePicker.innerButtonIndex
                        checkable: true

                        onClicked: {
                            timePicker.outerButtonIndex = -1
                            timePicker.innerButtonIndex = index
                            if (timePicker.useWorkTimes) {
                                timePicker.hrsDisplay = timePicker.timePickerDisplayModel[index].n
                            } else {
                                timePicker.hrsDisplay = timePicker.timePickerDisplayModel[index].c2
                            }
                            if (timePicker.autoSwapToMinutes) {
                                timePicker.onMinutesButtonClicked()
                            }
                        }

                        ButtonGroup.group: buttonGroup

                        property real angle: 360 * (index / innerRepeater.count)

                        transform: [
                            Translate {
                                y: -timeButtonsPane.width / 3 + innerButton.height / 2
                            },
                            Rotation {
                                angle: innerButton.angle
                                origin.x: innerButton.width / 2
                                origin.y: innerButton.height / 2
                            }
                        ]

                        contentItem: Label {
                            text: innerButton.text
                            font: innerButton.font
                            minimumPointSize: 8
                            fontSizeMode: Text.Fit
                            opacity: innerButton.checked ? 1.0 : enabled
                                                           || innerButton.highlighted ? 1.0 : 0.6
                            color: innerButton.checked
                                   || innerButton.highlighted ? Material.foreground : Material.color(
                                                                    Material.Grey,
                                                                    Material.Shade500)
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight
                            rotation: -innerButton.angle
                        } // content Label

                        background: Rectangle {
                            color: innerButton.checked ? Material.accent : "transparent"
                            radius: width / 2
                        }
                    } // inner button
                } // innerRepeater
            } // innerButtonsPane

            Repeater {
                id: outerRepeater
                model: timePicker.timePickerModel
                anchors.fill: parent
                delegate: Button {
                    id: outerButton
                    focusPolicy: Qt.NoFocus
                    text: timePicker.pickMinutes ? modelData.m : timePicker.useWorkTimes ? modelData.d : modelData.c1
                    font.bold: checked || timePicker.pickMinutes
                               && timePicker.onlyQuartersAllowed
                    width: 100
                    height: width
                    anchors.centerIn: parent
                    checked: index == timePicker.outerButtonIndex
                    checkable: true
                    enabled: timePicker.pickMinutes
                             && timePicker.onlyQuartersAllowed ? modelData.q : true

                    onClicked: {
                        timePicker.innerButtonIndex = -1
                        timePicker.outerButtonIndex = index
                        if (timePicker.pickMinutes) {
                            timePicker.minutesDisplay = timePicker.timePickerDisplayModel[index].m
                        } else {
                            if (timePicker.useWorkTimes) {
                                timePicker.hrsDisplay = timePicker.timePickerDisplayModel[index].d
                            } else {
                                timePicker.hrsDisplay = timePicker.timePickerDisplayModel[index].c1
                            }
                            if (timePicker.autoSwapToMinutes) {
                                timePicker.onMinutesButtonClicked()
                            }
                        }
                    }

                    ButtonGroup.group: buttonGroup

                    property real angle: 360 * (index / outerRepeater.count)

                    transform: [
                        Translate {
                            y: -timeButtonsPane.width / 2 + outerButton.height / 2
                        },
                        Rotation {
                            angle: outerButton.angle
                            origin.x: outerButton.width / 2
                            origin.y: outerButton.height / 2
                        }
                    ]

                    contentItem: Label {
                        text: outerButton.text
                        font: outerButton.font
                        minimumPointSize: 8
                        fontSizeMode: Text.Fit
                        opacity: enabled || outerButton.highlighted
                                 || outerButton.checked ? 1 : 0.3
                        color: outerButton.checked
                               || outerButton.highlighted ? Material.background : timePicker.pickMinutes && timePicker.onlyQuartersAllowed ? Material.background : "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                        rotation: -outerButton.angle
                    } // outer content label

                    background: Rectangle {
                        color: outerButton.checked ? Material.accent : "transparent"
                        radius: width / 2
                    }
                } // outer button
            } // outerRepeater

            Rectangle {
                // line to outer buttons
                opacity: timePicker.pickMinutes ? 1 : 0.5
                x: parent.width / 2 - width / 2
                y: parent.height / 2 - height
                width: 2
                height: timeButtonsPane.width / 3
                transformOrigin: Item.Bottom
                rotation: minutesArrowAngle
                color: Material.foreground
                antialiasing: true
            } // line to outer buttons
            Rectangle {
                // line to inner buttons
                opacity: timePicker.pickMinutes ? 0.5 : 1
                x: parent.width / 2 - width / 2
                y: parent.height / 2 - height
                width: 4
                height: timeButtonsPane.width / 4
                transformOrigin: Item.Bottom
                rotation: hrsArrowAngle
                color: Material.foreground
                antialiasing: true
            } // line to outer buttons

            Rectangle {
                // centerpoint
                anchors.centerIn: parent
                width: 10
                height: 10
                color: Material.foreground
                radius: width / 2
            }
        } // timeButtonsPane

        Pane {
            id: footerPane
            padding: 0

            implicitHeight: 40
            Layout.fillWidth: true
            background: Rectangle {
                color: "transparent"
            }
            ColumnLayout {
                anchors.fill: parent

                RowLayout {
                    Layout.fillHeight: true
                    Button {
                        Layout.fillWidth: true
                        Layout.preferredWidth: 1
                        text: qsTr("Cancel")
                        onClicked: {
                            timePicker.close()
                        }
                    } // cancel button
                    Item {
                        Layout.fillWidth: true
                        Layout.preferredWidth: 1
                    }
                    Button {
                        Layout.fillWidth: true
                        Layout.preferredWidth: 1
                        text: qsTr("OK")
                        onClicked: {
                            timePicker.isOK = true
                            timePicker.close()
                        }
                    } // ok button
                } // button row
            } // button col
        } // footer pane
    }
} // timePicker
