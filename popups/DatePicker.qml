// ekke (Ekkehard Gentz) @ekkescorner
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import Qt.labs.calendar 1.0
import QtQuick.Controls.Material 2.12

Rectangle {
    id: datePickerRoot
    visible: false
	property bool isLandscape: false
    property date selectedDate: new Date()
    property int displayMonth: selectedDate.getMonth()
    property int displayYear: selectedDate.getFullYear()
    property int calendarWidth: isLandscape? parent.width * 0.70 : parent.width * 0.85
    property int calendarHeight: isLandscape? appWindow.height * 0.94 : parent.height * 0.85
    property bool isOK: false

	signal onOpen()
	signal onClose(bool isOk)
        
    function open() {
        visible = true
        isOK = false
        onOpen()
    }
    
    function close() {
		visible = false
		onClose(isOK)
    }
    
    color: Material.background

    GridLayout {
        id: calendarGrid
        // column 0 only visible if Landscape
        columns: 3
        // row 0 only visible if Portrait
        rows: 5
        anchors.fill: parent

        Pane {
            id: portraitHeader
            visible: !isLandscape
            padding: 0
            Layout.columnSpan: 2
            Layout.column: 1
            Layout.row: 0
            Layout.fillWidth: true
            Layout.fillHeight: true
            background: Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: Material.background
            }
            ColumnLayout {
                anchors.verticalCenter: parent.verticalCenter
                spacing: 6
                Label {
                    topPadding: 12
                    leftPadding: 24
                    font.pointSize: 18
                    text: datePickerRoot.displayYear
                    color: Material.foreground
                    opacity: 0.8
                }
                Label {
                    leftPadding: 24
                    bottomPadding: 12
                    font.pointSize: 36
                    text: Qt.formatDate(datePickerRoot.selectedDate, "ddd")+", "+Qt.formatDate(datePickerRoot.selectedDate, "d")+". "+Qt.formatDate(datePickerRoot.selectedDate, "MMM")
                    color: Material.foreground
                }
            }
        } // portraitHeader

        Pane {
            id: landscapeHeader
            visible: isLandscape
            padding: 0
            Layout.column: 0
            Layout.row: 0
            Layout.rowSpan: 5
            Layout.fillWidth: true
            Layout.fillHeight: true
            background: Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: Material.background
            }
            ColumnLayout {
                spacing: 6
                Label {
                    topPadding: 12
                    leftPadding: 24
                    rightPadding: 24
                    font.pointSize: 18
                    text: datePickerRoot.displayYear
                    color: Material.foreground
                    opacity: 0.8
                }
                Label {
                    leftPadding: 24
                    rightPadding: 24
                    font.pointSize: 36
                    text: Qt.formatDate(datePickerRoot.selectedDate, "ddd")
                    color: Material.foreground
                }
                Label {
                    leftPadding: 24
                    rightPadding: 24
                    font.pointSize: 36
                    text: Qt.formatDate(datePickerRoot.selectedDate, "d")+"."
                    color: Material.foreground
                }
                Label {
                    leftPadding: 24
                    rightPadding: 24
                    font.pointSize: 36
                    text: Qt.formatDate(datePickerRoot.selectedDate, "MMM")
                    color: Material.foreground
                }
            }
        } // landscapeHeader

        ColumnLayout {
            id: title
            Layout.columnSpan: 2
            Layout.column: 1
            Layout.row: 1
            Layout.fillWidth: true
            spacing: 6
            RowLayout {
                height: implicitHeight * 2
                spacing: 6
                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: "<"
                    onClicked: {
                        if(datePickerRoot.displayMonth > 0) {
                            datePickerRoot.displayMonth --
                        } else {
                            datePickerRoot.displayMonth = 11
                            datePickerRoot.displayYear --
                        }
                    }
                }
                Label {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 3
                    text: monthGrid.title
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pointSize: 18
                }
                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: ">"
                    onClicked: {
                        if(datePickerRoot.displayMonth < 11) {
                            datePickerRoot.displayMonth ++
                        } else {
                            datePickerRoot.displayMonth = 0
                            datePickerRoot.displayYear ++
                        }
                    }
                }
            } // row layout title
        } // title column layout

        // TODO not working in dark theme
        DayOfWeekRow {
            id: dayOfWeekRow
            Layout.column: 2
            Layout.row: 2
            rightPadding: 24
            Layout.fillWidth: true
            font.bold: false
            delegate: Label {
                    text: model.shortName
                    font: dayOfWeekRow.font
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
        } // day of weeks

        // TODO not working in dark theme
        WeekNumberColumn {
            id: weekNumbers
            Layout.column: 1
            Layout.row: 3
            Layout.fillHeight: true
            leftPadding: 24
            font.bold: false
            month: datePickerRoot.displayMonth
            year: datePickerRoot.displayYear
            delegate: Label {
                    text: model.weekNumber
                    font: weekNumbers.font
                    //font.bold: false
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
        } // WeekNumberColumn


        MonthGrid {
            id: monthGrid
            Layout.column: 2
            Layout.row: 3
            Layout.fillHeight: true
            Layout.fillWidth: true
            rightPadding: 24

            month: datePickerRoot.displayMonth
            year: datePickerRoot.displayYear

            // ATTENTION: on Qt 5.9 clicked signal only if clicked with mouse
            // no event if tapped on a day
            // https://bugreports.qt.io/browse/QTBUG-61585
            // fixed in 5.9.2
            // so as a woraround I added a MouseArea for the delegate Label
//            onClicked: {
//                // Important: check the month to avoid clicking on days outside where opacity 0
//                if(date.getMonth() == datePickerRoot.displayMonth) {
//                    datePickerRoot.selectedDate = date
//                    console.log("tapped on a date ")
//                } else {
//                    console.log("outside valid month "+date.getMonth())
//                }
//            }

            delegate: Label {
                id: dayLabel
                readonly property bool selected: model.day === datePickerRoot.selectedDate.getDate()
                                                 && model.month === datePickerRoot.selectedDate.getMonth()
                                                 && model.year === datePickerRoot.selectedDate.getFullYear()
                text: model.day
                font.bold: model.today? true: false
                opacity: model.month === monthGrid.month ? 1 : 0
                color: pressed || selected ? Material.foreground : model.today ? Material.accent : Material.foreground
                minimumPointSize: 8
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                background: Rectangle {
                    anchors.centerIn: parent
                    width: Math.min(parent.width, parent.height) * 1.2
                    height: width
                    radius: width / 2
                    color: Material.accent
                    visible: pressed || parent.selected
                }
                // WORKAROUND !! see onClicked()
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("mouse as click")
                        // Important: check the month to avoid clicking on days outside where opacity 0
                        if(date.getMonth() == datePickerRoot.displayMonth) {
                            datePickerRoot.selectedDate = date
                            console.log("tapped on a date ")
                        } else {
                            console.log("outside valid month "+date.getMonth())
                        }
                    }
                } // mouse
            } // label in month grid
        } // month grid


        ColumnLayout {
            Layout.column: 2
            Layout.row: 4
            id: footer
            Layout.fillWidth: true
            RowLayout {
                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 3
                    text: qsTr("Today")
                    onClicked: {
                        datePickerRoot.selectedDate = new Date()
                        datePickerRoot.displayMonth = datePickerRoot.selectedDate.getMonth()
                        datePickerRoot.displayYear = datePickerRoot.selectedDate.getFullYear()
                    }
                } // cancel button
                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 3
                    text: qsTr("Cancel")
                    onClicked: {
                        datePickerRoot.close()
                    }
                } // cancel button
                Button {
                    Layout.fillWidth: true
                    Layout.preferredWidth: 2
                    text: qsTr("OK")
                    onClicked: {
                        datePickerRoot.isOK = true
                        datePickerRoot.close()
                    }
                } // ok button
            }
        } // footer buttons
    } // grid layout
} // popup calendar
