// ekke (Ekkehard Gentz) @ekkescorner
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Label {
    Layout.fillWidth: true
    font.pixelSize: fontSizeDisplay3
    opacity: opacityDisplay3
}
